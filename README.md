# CoinGecko

Take home assignment answers.

## Branches
There are 2 branches in this repository

### master
Cointains only the answers to the 4 questions inside `answers.md`

### dashboard
The code to build the cryptocurrency dashboard.
(https://cryptocurrency-dashboard.vercel.app/)