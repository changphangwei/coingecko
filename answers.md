# Internship Interview Questions

## | Question 1
Without using the collection *ArrayList*, we can use a **static** array to make a dynamic array. This is actually the underlying mechanism of *ArrayList*. We can create a class called `dynamicArray` with the attributes of a static array, `int` length and `int` capacity.

> This concept is also applied similarly in *HashTables*. HashTables often need to be resized to maintain *O(1)* time complexity. The difference is HashTables resize when the *load factor* has been exceeded, instead of when the table is filled.

### Steps:
1. Initialize a static array with an initial capacity when the class is instantiated.
2. Add the elements into the static array while keeping track of the number of elements.
3. If adding the next element will exceed the capacity (length>capacity):
    - Create a new static array with twice the size of the original array.
    - Copy all the original elements into the new array.
    - Add the new element into the array.

### Code
    public void add(T elem) {
            // Time to resize!
            if (length + 1 >= capacity) {
                capacity *= 2; // double the size
                T[] new_arr = (T[]) new Object[capacity];
                for (int i = 0; i < length; i++) 
                    new_arr[i] = arr[i];
                arr = new_arr;
            }

            arr[length++] = elem;
        }

### Using linkedList
Instead of using a static array, we can implement dynamicArrays using `linkedList` as the underlying storage. Therefore, there is no need to track the capacity of the array anymore.

## | Question 2
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
        }
    }
The Big-O notation for the code would be **O(n<sup>2</sup>)**. This means that the performance of the algorithm will be less than or equal to a quadratic curve. The performance will never exceed the quadratic curve.

### Analysis
1. The outer loop for index `i` will iterate ***at most n*** times.
2. The inner loop for index `j` will iterate ***at most n*** times for each `i` value. This is because both are comparing to the same size.
3. Using the theorem of simplification, O(n*n) = O(n<sup>2</sup>).
> f(n) = O(n)
>
> g(n) = O(n)
>
> f(n)g(n) = O(n*n)

### Visualization
![bigo-visual](/assets/n2.png)

## | Question 3
Since the website actually loads, this means that code showing the _list of items_ is the issue. Hence, the first approach should be checking for any syntax errors inside the code of the component.

If the syntax looks fine, then it could possibly runtime error. Runtime error covers logic error too. Therefore, the functions that are called when creating the _list of items_ component needs to be checked. Here, it is important to check the objects or parameters that are passed. We can use `console.log` to see the contents of the variable, to make sure it is the right content. For example, a _list of item_ is usually constructed using mapping and destructuring of an array of objects. Hence, we need to check the the content of the object.

If not, it could possibly be the customer's browser that cannot support the code. More complex situations will require debugging tools to aid the process.

## | Question 4
The simple answer is no, not all software should be built using C/C++. With Java being based off C++, having similar syntax and being a typed language, both languages cater to different situations.In the following sections, we will analyze why.

### Why C++?
The main benefit of using C++ is definitely **speed**. This is because C++ can be a low-level language. It has the ability to manipulate computer resources: mainly memory management; to include what is needed and remove what is not needed. Not only that, C++ is a compiled language. Meaning, the program is converted to machine language, which offers much faster speed. This is proven true if we look at examples of C++ applications: Games, GUI based application, web browsers, operating systems, etc. As shown, all these software requires fast execution speed. Therefore, C++ will be the right choice.

#### Drawbacks of C++
1. Application is platform specific. The code has to be re-compiled to run.
2. Security issues.
3. Steep learning curve.
4. Limited libraries compared to Java.

### Why not C++?
Using Java as the main example, **portability** is the top reason on why not to use C++. Java's slogan has always been _"Write once, run anywhere"_. This is possible because Java uses both compiler and interpreter (JVM). As a result, the application does not have to consider to platform to run. This is also a reason why Java is the foundation of developing Android mobile applications. Not only that, Java has tons of libraries and APIs that can be used in developing the application.

Java is also a much easier language to learn compared to C++. Since Java takes care of issues like pointers, garbage collection and threading, the developers can focus on building the application, without putting much focus on resource management.

### Summary
It is possible to implement C++ everywhere, but there is no such need to do so. If the application does not rely heavily on speed, then other languages like Java would suffice.

> How I see it: Imagine writing a to-do list application using C++. Is there a need to display the to-dos within 0.05s? 